require_relative '../bowling_game.rb'

describe BowlingGame do
  let(:game) { BowlingGame.new }

  before(:each) do
    game.roll_frames(frames) 
  end

  describe "#score" do
    subject { game.score }

    context "seemingly random rolls" do
      let(:frames) { "16_18_24_72_23_X_16_15_16_24" }
      it { should eql 79 }
    end

    context "a single spare" do
      let(:frames) { "0/_00_00_00_00_00_00_00_00_00" }
      it { should eql 10 }
    end

    context "multiple strikes in sequence" do
      let(:frames) { "42_X_32_02_X_X_X_X_50_00" }
      it { should eql 133 }
    end

    context "multiple spares in sequence" do
      let(:frames) { "4/_4/_32_0/_7/_8/_2/_20_80_20" }
      it { should eql 103 }
    end

    context "multiple strikes not in sequence" do
      let(:frames) { "X_23_X_71_X_09_11_12_X_53" }
      it { should eql 105 }
    end

    context "multiple spares not in sequence" do
      let(:frames) { "4/_21_7/_30_2/_45_6/_23_8/_71" }
      it { should eql 96 }
    end

    context "multiple strikes and spares" do
      let(:frames) { "4/_X_3/_63_X_9/_X_1/_X_23" }
      it { should eql 165 }
    end

    context "during last frame" do
      context "all strikes" do
        let(:frames) { "00_00_00_00_00_00_00_00_00_XXX" }
        it { should eql 30 }
      end

      context "no strikes or spares" do
        let(:frames) { "00_00_00_00_00_00_00_00_00_21" }
        it { should eql 3 }
      end

      context "strike and a spare" do
        let(:frames) { "00_00_00_00_00_00_00_00_00_X2/" }
        it { should eql 20 }
      end

      context "two strikes" do
        let(:frames) { "00_00_00_00_00_00_00_00_00_XX2" }
        it { should eql 22 }
      end
    end
  end
end
