require_relative 'frame.rb'

class BowlingGame
  def initialize
    @frames = []
  end

  def score
    return 300 if rolls.uniq == ["X"]
    @frames.size.times.inject(0) do |score, idx|
      score + increase_score([frame(idx), frame(idx+1), frame(idx+2)])
    end
  end

  def roll(rolls)
    rolls.inject([]) do |acc, val|
      if val == "X"
        @frames << Frame.new(val)
        @frames << Frame.new(acc) unless acc.empty?
        []
      else
        acc << val
      end
    end
  end

  def roll_frames(frames)
    parse_frames(frames)
  end

  private

  def raw_score(char)
    (char =~ /[0-9]/) ? char.to_i : 10
  end

  def rolls
    @frames.map(&:rolls).flatten
  end

  def parse_frames(frames)
    @frames = frames.split(/_/).map { |f| Frame.new(*f.split('')) }
  end

  def frame(idx)
    @frames[idx] || Frame.new(0,0)
  end

  def increase_score(frames, bonus = 0)
    if frames[0].bonus_throw?
      return frames[0].rolls.inject([]) { |acc, r|
        acc[-1] = 0 if r == "/"
        acc << raw_score(r)
      }.inject(:+)
    elsif frames[0].strike?
      bonus = frames[1].score
      bonus += frames[2].score if frames[1].strike?
    elsif frames[0].spare?
      bonus = raw_score(frames[1].first_roll)
    end
    frames[0].score + bonus
  end
end
