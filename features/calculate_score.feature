Feature: Calculate score
  I want to calculate my score at the end of a game
  As an up-and-coming professional bowler
  So I can find out if I have won

  Scenario: I have just finished a game
    When I provide a valid sequence of rolls
    Then I should receive my result
    And the score should be realistic

  Scenario: Didn't score anything
    When I haven't knocked any pins down
    Then the score should be 0

  Scenario: Got the best possible score
    When I strike on every frame
    Then the score should be 300
