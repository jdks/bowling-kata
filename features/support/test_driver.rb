require_relative '../../bowling_game.rb'

module TestDriver
  def roll_sequence(roll, count)
    @game = BowlingGame.new
    @game.roll([roll.to_s] * count)
  end

  def current_score
    @game.score
  end

  def max_score
    300
  end

  def min_score
    0
  end
end
