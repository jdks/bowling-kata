require_relative '../test_driver.rb'

When(/^I provide a valid sequence of rolls$/) do
  roll_sequence("1", 20)
end

When(/^I haven't knocked any pins down$/) do
  roll_sequence("0", 20)
end

When(/^I strike on every frame$/) do
  roll_sequence("X", 12)
end

Then(/^I should receive my result$/) do
  expect( current_score ).to be_kind_of(Fixnum)
end

Then(/^the score should be realistic$/) do
  expect( current_score ).to be >= min_score
  expect( current_score ).to be <= max_score
end

Then(/^the score should be (\d+)$/) do |expected_score|
  expect( current_score ).to be expected_score
end

Transform /^(-?\d+)$/ do |number|
  number.to_i
end

World(TestDriver)
