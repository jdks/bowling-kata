class Frame
  attr_reader :rolls

  def initialize(*args)
    @rolls = args
  end

  def first_roll
    @rolls.first
  end

  def second_roll
    @rolls[1]
  end

  def third_roll
    @rolls[2]
  end

  def strike?
    first_roll == "X"
  end

  def spare?
    second_roll == "/"
  end

  def score
    (strike? || spare?) ? 10 : @rolls.map(&:to_i).inject(:+)
  end

  def bonus_throw?
    @rolls.size == 3
  end

  def to_s
    "[#{first_roll}, #{second_roll}]"
  end
end
